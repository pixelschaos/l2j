#!/bin/bash
#
### BEGIN INIT INFO
# Provides:          L2J_Server
# Required-Start:    $remote_fs $syslog mysql
# Required-Stop:     $remote_fs $syslog
# Should-Start:      $network $time
# Should-Stop:       $network $time
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: start, stop or restart L2J_Server
# Description:       start, stop or restart L2J_Server
### END INIT INFO 
#
# Change xxxx with your home directory server. Absolute path.
base=/Users/chris/Documents/Projects/L2J

start_l2j() {
	if [ -n "$(pgrep -f GameServer_loop.sh)" -o "$(pgrep -f l2jserver.gameserver)" -o "$(pgrep -f LoginServer_loop.sh)" -o "$(pgrep -f l2jserver.loginserver)" ]; then
		echo ============================================================
		echo Try first to stop all L2J server scripts and java processes.
		echo Use stop/restart command.
		echo ============================================================
	else
		cd ${base}/login
		./startLoginServer.sh
		cd ${base}/gameserver
		./startGameServer.sh
		echo =======================================================
		echo Game Server processes started.
		echo You have to wait about 100 sec or more to be operable.
		echo Give next command to see progress:
		echo tail -f ${base}/gameserver/log/stdout.log
		echo Stop tail command with CTRL-C
		echo =======================================================
	fi
}

stop_l2j() {
	#Stop Game Server script and java
	if [ -n "$(pgrep -f GameServer_loop.sh)" ]; then
		pkill -f GameServer_loop.sh
		echo Game Server Script stopped
	else
		echo ==================================
		echo Game Server Script is not running!
		echo ==================================
	fi
	if [ -n "$(pgrep -f gameserver)" ]; then
		pkill -f gameserver
		echo Waiting for Game Java Server to exit...
		i=1
			while [ -n "$(pgrep -f gameserver)" ]
				do
					sleep 1
					i=$((i+1))
				done
	echo Game java server stopped in $i sec
	else
		echo ================================
		echo Game java server is not running!
		echo ================================
	fi
	#Stop login server script and java
	if [ -n "$(pgrep -f LoginServer_loop.sh)" ]; then
		pkill -f LoginServer_loop.sh
		echo Login Server Script stopped
	else
		echo ===================================
		echo Login Server Script is not running!
		echo ===================================
	fi
	if [ -n "$(pgrep -f loginserver)" ]; then
		pkill -f loginserver
		echo Waiting for Login Java Server to exit...
		i=1
			while [ -n "$(pgrep -f loginserver)" ]
				do
					sleep 1
					i=$((i+1))
				done
	echo Login java server stopped in $i sec
	else
		echo =================================
		echo Login Java Server is not running!
		echo =================================
	fi
}

case "$1" in
	start)
		start_l2j
	;;
	stop)
		stop_l2j
	;;
	restart)
		stop_l2j
		start_l2j
	;;
	deletelogs)
		rm -R ${base}/gameserver/log/* && sudo rm -R ${base}/login/log/* && sudo rm -R ${base}/libs/cachedir/
	;;
	*)
	echo "Usage: $0 start|stop|restart|deletelogs" >&2
	exit 3
	;;
esac

